//= ../../bower_components/jquery/dist/jquery.js

$('.scroll-to-top').click(function() {
    $("html,body").animate({ scrollTop: $("body").offset().top }, 600);
});

$('.sorting_date').click(function() {
    var direction = $(this).find('.direction');
    if (direction.hasClass('invisible')) {
        direction.removeClass('invisible');
        direction.addClass('show');
    } else {
        direction.toggleClass('reverse');
    }
    $('.sorting_ready .direction').addClass('invisible');
    $('.sorting_ready .direction').removeClass('show');
});

$('.sorting_ready').click(function() {
    var direction = $(this).find('.direction');
    if (direction.hasClass('invisible')) {
        direction.removeClass('invisible');
        direction.addClass('show');
    } else {
        direction.toggleClass('reverse');
    }
    $('.sorting_date .direction').addClass('invisible');
    $('.sorting_date .direction').removeClass('show');
});